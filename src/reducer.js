// @flow

/* eslint no-unused-vars: "off" */

import { Logger } from 'aws-amplify'
import type { State, Actions, SetViewportAction, AddImagePointAction, AddMapPointAction, SetImageSizeAction, SetImageAction, SetSelectPointAction } from './types'
import { viewport } from './config.json' // flowlint-line untyped-import:off

const logger = new Logger('reducer', 'INFO')
const defaultState = {
  viewport,
  image: null,
  imagePoints: [],
  imageSize: null,
  mapPoints: [],
  selectPoint: 0,
}

const actions = {
  'SET_VIEWPORT': (state: State, action: SetViewportAction) => {
    return { ...state, viewport: action.viewport }
  },
  'SET_SELECT_POINT': (state: State, action: SetSelectPointAction) => {
    return { ...state, selectPoint: action.selectPoint }
  },
  'SET_IMAGE': (state: State, action: SetImageAction) => {
    return { ...state, image: action.image }
  },
  'SET_IMAGE_SIZE': (state: State, action: SetImageSizeAction) => {
    return { ...state, imageSize: action.imageSize }
  },
  'ADD_IMAGE_POINT': (state: State, action: AddImagePointAction) => {
    const imagePoints = [ ...state.imagePoints ]
    imagePoints[action.selectPoint] = action.imagePoint
    return { ...state, imagePoints }
  },
  'ADD_MAP_POINT': (state: State, action: AddMapPointAction) => {
    const mapPoints = [ ...state.mapPoints ]
    mapPoints[action.selectPoint] = action.mapPoint
    return { ...state, mapPoints }
  },
}

const reducer = function(state: State = defaultState, action: Actions): State {
  // logger.info(action)
  switch (action.type) {
    case 'SET_VIEWPORT':
      return actions[action.type](state, action)
    case 'SET_SELECT_POINT':
      return actions[action.type](state, action)
    case 'SET_IMAGE':
      return actions[action.type](state, action)
    case 'ADD_IMAGE_POINT':
      return actions[action.type](state, action)
    case 'SET_IMAGE_SIZE':
      return actions[action.type](state, action)
    case 'ADD_MAP_POINT':
      return actions[action.type](state, action)
    default:
      return state
  }
}

export default reducer

