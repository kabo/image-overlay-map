export default img => {
  return new Promise((resolve, reject) => {
    try {
      const reader = new FileReader()
      reader.addEventListener('load', info => resolve(info.target.result))
      reader.readAsDataURL(img)
    } catch (err) {
      reject(err)
    }
  })
}
