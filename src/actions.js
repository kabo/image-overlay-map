// @flow

import type { Viewport, SetViewportAction, SetImageAction, ImagePoint, ImageSize, MapPoint, AddImagePointAction, AddMapPointAction, SetImageSizeAction, SetSelectPointAction } from './types'

export function setViewport(viewport: Viewport): SetViewportAction {
  return {
    type: 'SET_VIEWPORT',
    viewport
  }
}
export function setSelectPoint(selectPoint: number): SetSelectPointAction {
  return {
    type: 'SET_SELECT_POINT',
    selectPoint
  }
}
export function setImage(image: string): SetImageAction {
  return {
    type: 'SET_IMAGE',
    image
  }
}
export function addImagePoint(imagePoint: ImagePoint, selectPoint: number): AddImagePointAction {
  return {
    type: 'ADD_IMAGE_POINT',
    imagePoint,
    selectPoint
  }
}
export function setImageSize(imageSize: ImageSize): SetImageSizeAction {
  return {
    type: 'SET_IMAGE_SIZE',
    imageSize
  }
}
export function addMapPoint(mapPoint: MapPoint, selectPoint: number): AddMapPointAction {
  return {
    type: 'ADD_MAP_POINT',
    mapPoint,
    selectPoint
  }
}

