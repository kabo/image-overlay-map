// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Map, TileLayer, ImageOverlay, LayersControl } from 'react-leaflet'
import { Logger } from 'aws-amplify'
import { scaleLinear } from 'd3-scale'
import Row from 'antd/lib/row' // flowlint-line untyped-import:off
import Col from 'antd/lib/col' // flowlint-line untyped-import:off
import Upload from 'antd/lib/upload' // flowlint-line untyped-import:off
import Icon from 'antd/lib/icon' // flowlint-line untyped-import:off
import Radio from 'antd/lib/radio' // flowlint-line untyped-import:off
import type { Actions, Viewport, SetViewportAction, SetImageAction, MapPoints, ImageSize, ImagePoints, SetImageSizeAction, AddImagePointAction, AddMapPointAction, ImagePoint, MapPoint, SetSelectPointAction } from './types'
import { setViewport, setImage, addImagePoint, setImageSize, addMapPoint, setSelectPoint } from './actions'
import getBase64 from './getBase64' // flowlint-line untyped-import:off
import { linzKey } from './secrets.json' // flowlint-line untyped-import:off
const { BaseLayer } = LayersControl
const Dragger = Upload.Dragger
const RadioButton = Radio.Button
const RadioGroup = Radio.Group

const logger = new Logger('App', 'INFO')

type Props = {
  viewport: Viewport,
  image: string,
  mapPoints: MapPoints,
  imageSize: ImageSize,
  imagePoints: ImagePoints,
  selectPoint: number,
  setViewport: (viewport: Viewport) => SetViewportAction,
  setImage: (image: string) => SetImageAction,
  setImageSize: (imageSize: ImageSize) => SetImageSizeAction,
  addImagePoint: (point: ImagePoint, selectPoint: number) => AddImagePointAction,
  addMapPoint: (point: MapPoint, selectPoint: number) => AddMapPointAction,
  setSelectPoint: (selectPoint: number) => SetSelectPointAction,
}

class App extends Component<Props> {
  componentDidMount() {
    document.onkeypress = e => {
      const key = String.fromCharCode(e.charCode)
      if (key === '1') {
        this.props.setSelectPoint(0)
      }
      if (key === '2') {
        this.props.setSelectPoint(1)
      }
    }
  }
  render() {
    const { viewport, image, setViewport, setImage, imagePoints, imageSize, mapPoints, addMapPoint, addImagePoint, setImageSize, selectPoint, setSelectPoint } = this.props
    if (!viewport) {
      return <div className="App"></div>
    }
    const mapViewport = { ...viewport }
    const beforeUpload = async file => {
      const b64 = await getBase64(file)
      setImage(''+b64)
      return false
    }
    const mapClick = ({latlng}) => {
      const lon = latlng.lng
      const lat = latlng.lat
      addMapPoint({lon, lat}, selectPoint)
    }
    const imageClick = e => {
      const rect = e.currentTarget.getBoundingClientRect()
      const x = e.clientX - rect.left
      const y = e.clientY - rect.top
      addImagePoint({x, y}, selectPoint)
      const { width, height } = rect
      setImageSize({width, height})
    }
    let imageUrl, imageBounds, imageOpacity
    if (image && imagePoints.length === 2 && mapPoints.length === 2 && imageSize) {
      const x = scaleLinear()
        .domain([imagePoints[0].x, imagePoints[1].x])
        .range([mapPoints[0].lon, mapPoints[1].lon])
      const y = scaleLinear()
        .domain([imagePoints[0].y, imagePoints[1].y])
        .range([mapPoints[0].lat, mapPoints[1].lat])
      imageUrl = new Image()
      imageUrl.src = image
      imageOpacity = 0.5
      imageBounds = [
        [y(imageSize.height), x(0)],
        [y(0), x(imageSize.width)]
      ]
    }
    return (
      <div className="App">
        <div className="readme-link">
          <a href="https://gitlab.com/kabo/image-overlay-map">README</a>
        </div>
        <header className="App-header">
          <h1 className="App-title">Image Overlay Map</h1>
        </header>
        <Row>
          <Col span={12}>
            <div className="map-wrapper">
              <Map
                viewport={mapViewport}
                onViewportChanged={setViewport}
                style={{height: '100%'}}
                onClick={mapClick}
              >
                <LayersControl position="topleft">
                  <BaseLayer checked name="LINZ">
                    <TileLayer
                      attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors | <a href=&quot;https://data.linz.govt.nz/layer/93652-nz-10m-satellite-imagery-2017/webservices/&quot;>LINZ</a> | <a href=&quot;https://gitlab.com/kabo/image-overlay-map&quot;>Source</a>"
                      url={`http://tiles-a.data-cdn.linz.govt.nz/services;key=${linzKey}/tiles/v4/layer=51872/EPSG:3857/{z}/{x}/{y}.png`}
                      maxZoom={19}
                    />
                  </BaseLayer>
                  <BaseLayer name="OpenStreetMap">
                    <TileLayer
                      attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors | <a href=&quot;https://gitlab.com/kabo/image-overlay-map&quot;>Source</a>"
                      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                      maxZoom={19}
                    />
                  </BaseLayer>
                </LayersControl>
                {imageUrl && <ImageOverlay
                  url={imageUrl}
                  bounds={imageBounds}
                  opacity={imageOpacity}
                  key={new Date().toISOString()}
                />}
              </Map>
            </div>
          </Col>
          <Col span={12} className="right-column">
            <Row gutter={16}>
              <Col span={12}>
                <Dragger
                  name="file"
                  multiple={false}
                  accept=".png,.gif,.jpg"
                  beforeUpload={beforeUpload}
                  fileList={[]}
                >
                  <p className="ant-upload-drag-icon">
                    <Icon type="inbox" />
                  </p>
                  <p className="ant-upload-text">Click or drag image to this area to upload</p>
                </Dragger>
              </Col>
              <Col span={12}>
                Select point number&nbsp;
                <RadioGroup value={selectPoint} onChange={e => setSelectPoint(e.target.value)} size="small">
                  <RadioButton value={0}>1</RadioButton>
                  <RadioButton value={1}>2</RadioButton>
                </RadioGroup>
                <div className="info">
                  {imageBounds && <div>
                    Image bounds: <pre>{JSON.stringify(imageBounds)}</pre>
                  </div>}
                </div>
              </Col>
            </Row>
            <div>
              {image && <img src={image} onClick={imageClick} className="uploaded-image" alt="[uploaded]" />}
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    viewport: state.viewport,
    image: state.image,
    imageSize: state.imageSize,
    imagePoints: state.imagePoints,
    mapPoints: state.mapPoints,
    selectPoint: state.selectPoint,
  }
}
const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    setViewport: (viewport: Viewport) => dispatch(setViewport(viewport)),
    setImage: (image: string) => dispatch(setImage(image)),
    setImageSize: (imageSize: ImageSize) => dispatch(setImageSize(imageSize)),
    addImagePoint: (point: ImagePoint, selectPoint: number) => dispatch(addImagePoint(point, selectPoint)),
    addMapPoint: (point: MapPoint, selectPoint: number) => dispatch(addMapPoint(point, selectPoint)),
    setSelectPoint: (selectPoint: number) => dispatch(setSelectPoint(selectPoint)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App)
