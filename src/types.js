// @flow

export type Viewport = {
  +center: [number, number],
  +zoom: number,
}
export type MapPoint = {
  +lon: number,
  +lat: number,
}
export type MapPoints = Array<MapPoint>
export type ImagePoint = {
  +x: number,
  +y: number,
}
export type ImagePoints = Array<ImagePoint>
export type ImageSize = {
  +width: number,
  +height: number,
}
export type State = {
  +viewport: Viewport,
  +image: ?string,
  +imageSize: ?ImageSize,
  +imagePoints: ImagePoints,
  +mapPoints: MapPoints,
  +selectPoint: number,
}

export type SetViewportAction = { type: 'SET_VIEWPORT', viewport: Viewport }
export type SetImageAction = { type: 'SET_IMAGE', image: string }
export type SetSelectPointAction = { type: 'SET_SELECT_POINT', selectPoint: number }
export type SetImageSizeAction = { type: 'SET_IMAGE_SIZE', imageSize: ImageSize }
export type AddImagePointAction = { type: 'ADD_IMAGE_POINT', imagePoint: ImagePoint, selectPoint: number }
export type AddMapPointAction = { type: 'ADD_MAP_POINT', mapPoint: MapPoint, selectPoint: number }

export type Actions = SetViewportAction | SetImageAction | SetImageSizeAction | AddImagePointAction | AddMapPointAction | SetSelectPointAction
