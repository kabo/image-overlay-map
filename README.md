# Image Overlay Map

## Usage

1. Upload an image
1. Click a point on the image and the corresponding point on the map
1. Hit `2` on your keyboard
1. Click another point on the image and the corresponding point on the map
1. See the image on the map, with your two points aligned. You also get the image bounds in the top right corner of the screen, if you want to use the image in an ImageOverlay in Leaflet.

You can keep clicking on the map or the image to make minor adjustments.

## Changelog

### v0.1.0 (2018-06-04)

 - First release

## License

Released under [COIL 0.5](./LICENSE.md) ([Copyfree Open Innovation License](http://coil.apotheon.org/)).
